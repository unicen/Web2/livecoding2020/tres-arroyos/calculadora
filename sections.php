<?php

function showHome() {
    include_once('libreria/header.php');
  
    $html = '
        <form id="form-calc" method="GET">         
          <label>X:<input type="numeric" name="operandoX"  required/></label>
          <label>Y:<input type="numeric" name="operandoY" required/></label>
          <select required name="operacion">
            <option value="sumar">Sumar</option>
            <option value="restar">Restar</option>
            <option value="dividir">Dividir</option>
            <option value="multiplicar">Multiplicar</option>
          </select>
  
          <input type="submit" value="Calcular!"> 
        </form>
  
        <section id="resultado">
          
        </section>
        
        <script src="js/main.js"></script>
      </body>
      </html>
    ';
  
    echo $html;
  }

  function showPi() {
    include('libreria/header.php'); 

    $html = '
    <h1>El número PI</h1>
    <p>π (pi) es la relación entre la longitud de una circunferencia y su diámetro en geometría euclidiana.1​ Es un número irracional2​ y una de las constantes matemáticas más importantes. Se emplea frecuentemente en matemáticas, física e ingeniería. El valor numérico de π, truncado a sus primeras cifras, es el siguiente:</p>
    ';
      
    $html .= "<h3>" . pi() . "</h3>";
    $html .= '
        </body>
        </html>
    ';

    echo $html;
}

function getDev($name){
    $result = '';
    if (isset($name)) {
        switch ($name) {
            case 'mariano':
                $result =  "<h3>Mariano Martinez</h3><p>Lorem bla bla</p>";
                break;
            case 'noelia':
                $result =  "<h3>Noelia Carrizo</h3><p>Lorem bla</p>";
                break;
            case 'matty':
                $result =  "<h3>Matty Rust</h3><p>Lorem loremaaa bla</p>";
                break;
        }
    }
    return $result;
}

function showAbout($name = null) {
    include_once('libreria/header.php');

    $html = '

    <h1>Acerca de</h1>

    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet sunt, asperiores nulla dolor at quos! Libero a quas placeat doloremque explicabo! Culpa, totam. Expedita recusandae fugiat consequatur laudantium amet alias?</p>
    
    <h2>Desarrolladores</h2>
    <ul>
        <li><a href="about/mariano">Mariano Martinez</a></li>
        <li><a href="about/noelia">Noelia Carrizo</a></li>
        <li><a href="about/matty">Matty Rust</a></li> 
    </ul>'
        . getDev($name) .
'</body>
</html>';

    echo $html;
}